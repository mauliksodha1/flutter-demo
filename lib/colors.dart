import 'package:flutter/material.dart';

const primaryColor = Color(0xFF32b389);
const primaryLightColor = Color(0xFFB2EBF2);
const primaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFF6ce6b9), Color(0xFF32b389)],
);
const secondaryTextColor = Color(0xFF757575);
const primaryTextColor = Color(0xFF212121);
const divider = Color(0xFFBDBDBD);
const white = Color(0xFFFFFFFF);
const black = Color(0xFF000000);

const animationDuration = Duration(milliseconds: 200);