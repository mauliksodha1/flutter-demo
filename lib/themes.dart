import 'package:flutter/material.dart';
import 'package:flutter_example/size_config.dart';
import 'package:flutter_example/colors.dart';

ThemeData theme() {
  return ThemeData(
      scaffoldBackgroundColor: white,
      appBarTheme: appBarTheme(),
      textTheme: textTheme(),
      primaryColor: primaryColor,
      primaryColorDark: primaryColor,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      buttonTheme: ButtonThemeData(
        buttonColor: primaryColor,
        highlightColor: Colors.greenAccent,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      ));
}

TextTheme textTheme() {
  return TextTheme(
    bodyText1: TextStyle(color: primaryTextColor),
    bodyText2: TextStyle(color: secondaryTextColor),
  );
}

AppBarTheme appBarTheme() {
  return AppBarTheme(
    color: white,
    elevation: 0,
    brightness: Brightness.light,
    iconTheme: IconThemeData(color: black),
    textTheme: TextTheme(
      headline6: TextStyle(color: primaryTextColor, fontSize: 18),
    ),
  );
}

TextStyle primaryBoldTextStyle() {
  return TextStyle(
    color: primaryTextColor,
    fontWeight: FontWeight.bold,
    fontSize: getProportionateScreenWidth(18),
  );
}

TextStyle primaryNormalTextStyle() {
  return TextStyle(
    color: primaryTextColor,
    fontWeight: FontWeight.normal,
    fontSize: getProportionateScreenWidth(14),
  );
}

TextStyle secondaryNormalTextStyle() {
  return TextStyle(
    color: secondaryTextColor,
    fontWeight: FontWeight.normal,
    fontSize: getProportionateScreenWidth(14),
  );
}

TextStyle secondaryBoldTextStyle() {
  return TextStyle(
    color: secondaryTextColor,
    fontWeight: FontWeight.bold,
    fontSize: getProportionateScreenWidth(18),
  );
}


TextStyle primaryNormalTextButtonStyle() {
  return TextStyle(
    color: white,
    fontWeight: FontWeight.normal,
    fontSize: getProportionateScreenWidth(18),
  );
}
