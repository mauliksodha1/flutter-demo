import 'package:flutter/material.dart';
import 'package:flutter_example/themes.dart';
import 'package:flutter_example/strings.dart';
import 'package:flutter_example/routes.dart';
import 'package:flutter_example/ui/splash/splash_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: flutterDemo,
      theme: theme(),
      /*home: Scaffold(
          appBar: AppBar(
            title: Text(flutterDemo),
          ),
          body: Center(
            child: Text(helloWorld),
          ),
        ));*/
      initialRoute: SplashScreen.routeName,
      routes: routes,
    );
  }
}
