const String helloWorld = 'Hello World';

const String flutterDemo = 'Flutter Demo';
const String signIn = 'Sign In';
const String signUp = 'Sign up';
const String toContinueWithFlutterDemo = 'to continue with Flutter Demo';
const String enterUserName = 'Enter your username';
const String forgotPassword = 'Forgot password?';
const String enterPassword = 'Enter your password';
const String doesNotHaveAccount = 'Does not have account ?';
