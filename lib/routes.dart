import 'package:flutter/material.dart';
import 'package:flutter_example/ui/signIn/sign_in_screen.dart';
import 'package:flutter_example/ui/splash/splash_screen.dart';

final Map<String, WidgetBuilder> routes = {
  SplashScreen.routeName: (context) => SplashScreen(),
  SignInScreen.routeName: (context) => SignInScreen()
};
