import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_example/colors.dart';
import 'package:flutter_example/strings.dart';
import 'package:flutter_example/themes.dart';
import 'package:flutter_example/size_config.dart';

class SignInBody extends StatefulWidget {
  @override
  _SignInBodyState createState() => _SignInBodyState();
}

class _SignInBodyState extends State<SignInBody> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SizedBox(
        width: double.infinity,
        child: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: getProportionateScreenWidth(20)),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: MediaQuery.of(context).size.height * 0.12),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    signIn,
                    style: primaryBoldTextStyle(),
                    textAlign: TextAlign.left,
                  ),
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    toContinueWithFlutterDemo,
                    style: secondaryNormalTextStyle(),
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.20),
                TextField(
                  textInputAction: TextInputAction.next,
                  maxLines: 1,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    labelText: "UserName",
                    border: OutlineInputBorder(),
                    hintText: enterUserName,
                  ),
                ),
                SizedBox(height: getProportionateScreenHeight(20)),
                TextField(
                  obscureText: true,
                  textInputAction: TextInputAction.done,
                  maxLines: 1,
                  decoration: InputDecoration(
                    labelText: "Password",
                    border: OutlineInputBorder(),
                    hintText: enterPassword,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                Container(
                  alignment: Alignment.centerRight,
                  child: Text(
                    forgotPassword,
                    style: secondaryNormalTextStyle(),
                    textAlign: TextAlign.left,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.02),
                Container(
                  height: 50,
                  width: double.infinity,
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                  child: RaisedButton(
                    textColor: white,
                    child: Text(
                      signIn,
                      style: primaryNormalTextButtonStyle(),
                    ),
                    onPressed: () {
                      // On Button Click
                    },
                    disabledTextColor: Colors.white,
                  ),
                ),
                Container(
                  child: Row(
                    children: <Widget>[
                      Text(
                        doesNotHaveAccount,
                        style: secondaryNormalTextStyle(),
                      ),
                      FlatButton(
                        textColor: Colors.blue,
                        child: Text(
                          signUp,
                          style: TextStyle(fontSize: 16),
                        ),
                        onPressed: () {
                          //signup screen
                        },
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
