import 'package:flutter/material.dart';
import 'package:flutter_example/size_config.dart';
import 'package:flutter_example/ui/splash/components/splash_body.dart';

class SplashScreen extends StatelessWidget {
  static String routeName = "/splash";

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
      body: SplashBody(),
    );
  }
}
